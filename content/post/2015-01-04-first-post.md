---
title: First post!
date: 2015-01-05
---
## Front Page Content

This is the project page of [GoShortUrl](https://gitlab.com/goshorturl/goshorturl).

The Shorturl service Url to Shortulr Cloud or u2s.cloud is using [GoShortUrl](https://gitlab.com/goshorturl/goshorturl)

GoShortUrl features:
* [x] Can be Shelf-hosted
* [x] Easy to Setup
* [x] It is Standalone
* [x] Costume Shorturls
* [ ] Redirect based on User-Agent and/or OS